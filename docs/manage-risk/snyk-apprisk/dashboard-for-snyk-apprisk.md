# Dashboard for Snyk AppRisk

## Overview

On the Snyk AppRisk dashboard page, you can add widgets that display an overview of your application and security controls.

You can customize the dashboard widgets as desired. Choose to rename or modify display configurations, add multiple widget instances, or remove widgets.

## Configure a widget

Customize your dashboard with the available widgets. You can change the settings of an existing widget or change the way it is displayed.

### Configure a widget

Customize your widgets as desired. You have the ability to move a widget around the dashboard, rename it, display or hide the legend, view it in full screen, export or download it, and even delete it entirely from your dashboard.

#### Settings menu

You can make several changes to a widget. All widgets allow you to change the name. Other particular settings are available for each widget. You can access the Settings menu by following these steps:

1. Select a widget and click the **Setting** menu.
2. &#x20;Customize the widget by changing its name or other specific details.&#x20;
3. After all changes are done, click **Apply**.

#### Action menu

Access the full list of general options from the action menu. You can access the Action menu by following these steps:

1. Select a widget and click the **Action** menu.
2. Select one of the following actions:
   * View in full screen
   * CSV export
   * XLS export
   * Download PNG
   * Download PDF
   * Remove widget
3. If you choose to remove a widget, you need to confirm your action and click **Remove** when the **Are you sure** pop-up is displayed.

<figure><img src="../../.gitbook/assets/image.png" alt="AppRisk - Action menu"><figcaption><p>AppRisk - Action menu</p></figcaption></figure>



\
